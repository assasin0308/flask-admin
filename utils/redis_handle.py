# -*- coding:utf-8 -*-
# @Time: 2020/8/31 0031 10:54
# @Author: assasin
# @Email: assasin0308@sina.com


# pip install redis
import redis
from db_config import REDIS_DB


class RedisHelper(object):

    def __init__(self,redis_conn):
        try:
            if redis_conn.get('auth'):
                self.__redis = redis.StrictRedis(
                    host=redis_conn.get('host'),
                    port=redis_conn.get('port'),
                    db=redis_conn.get('db_name'),
                    password=redis_conn.get('auth')
                )
            else:
                self.__redis = redis.StrictRedis(
                    host=redis_conn.get('host'),
                    port=redis_conn.get('port'),
                    db=redis_conn.get('db_name'),
                )

        except Exception as connect_error:
            raise connect_error
            exit(1)

    # 设置不过期的键值对
    def set(self,key,value):
        return  self.__redis.set(key,value)


    # 设置带过期时间的键值对 单位: 秒
    def setx(self,key,value,expire):
        return self.__redis.setex(key,expire,value)

    # 获取键的值
    def get(self,key):
        if self.__redis.exists(key):
            return self.__redis.get(key)
        else:
            return ""

    # 获取键的过期时间  返回 -1 : 键不存在,已失效
    def get_expire_time(self,key):
        if self.__redis.exists(key):
            return self.__redis.ttl(key)
        else:
            return -1

    # 批量设置键值对
    def multi_set(self,str_dict):
        if isinstance(str_dict,dict):
            return  self.__redis.mset(str_dict)
        else:
            return False

    # 批量获取键的值
    def multi_get(self,*keys):
        return self.__redis.mget(*keys)

    # 创建 hash 表
    def hash(self,hash_name,key,value):
        return self.__redis.hset(hash_name,key,value)

    def multi_hash(self,hash_name,str_dict):
        if isinstance(str_dict,dict):
            return self.__redis.hmget(hash_name,str_dict)
        else:
            return False

    # 获取hash表所有键值
    def hgetall(self,hash_name):
        return self.__redis.hgetall(hash_name)

    # 获取hash表指定键值
    def hget(self,hash_name,key):
        return self.__redis.hget(hash_name,key)

    # 获取hash表多个键值
    def hmget(self,hash_name,*keys):
        return self.__redis.hmget(hash_name,*keys)


    """ 其他操作方法自行扩展即可 """


# redis_client = RedisHelper(REDIS_DB)
# print(redis_client.multi_get('k1','k3'))
# print(redis_client.get_expire_time('my_name'))