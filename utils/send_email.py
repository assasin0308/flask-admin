# -*- coding:utf-8 -*-
# @Time: 2020/8/27 0027 9:49
# @Author: assasin
# @Email: assasin0308@sina.com

import smtplib
from email.mime.text import MIMEText

class Send_email(object):
    __smtp_obj = None

    def __init__(self,sender,receivers,title,content,is_html=False):
        self.mail_host = "smtp.exmail.qq.com"   # SMTP服务器
        self.mail_user = "shibin@growlib.com"     # 用户名
        self.mail_passwd = "19920308assasinB!"   # 密码(授权码)
        self.SSL_Port = 465             # SSL发信, 端口是465
        self.sender = sender            # 发件人邮箱
        self.receivers = receivers     # 接收人邮箱  列表
        self.title = title          # 邮件主题
        self.content = content       # 内容, 格式, 编码
        if is_html :  # 发送HTML文本邮件内容
            self.message = MIMEText(self.content,'html','utf-8')
        else:       # 普通文本邮件内容
            self.message = MIMEText(self.content,'plain','utf-8')

        self.message['From'] = "{}".format(self.sender)
        self.message['To'] = ",".join(self.receivers)
        self.message['Subject'] = self.title


    def send(self):
        try:
            self.__smtp_obj = smtplib.SMTP_SSL(self.mail_host,self.SSL_Port)                  # 启用SSL发信
            self.__smtp_obj.login(self.mail_user, self.mail_passwd)                           # 登录验证
            self.__smtp_obj.sendmail(self.sender,self.receivers,self.message.as_string())     # 发送
        except Exception as e:
            pass
            print(e)


send_obj = Send_email('shibin@growlib.com',['lihaixin@growlib.com','assasin0308@sina.com'],'这是邮件主题','<h1>这是发送的邮件内容</h1>',True)
send_obj.send()
