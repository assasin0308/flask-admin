# -*- coding:utf-8 -*-
# @Time: 2020/8/31 0031 10:49
# @Author: assasin
# @Email: assasin0308@sina.com

import hashlib
import binascii
from pyDes import des,CBC,PAD_PKCS5
import base64
from Crypto.Cipher import AES



"""加密/解密"""
class EncryptStr(object):
    PUBLIC_KEY = 'xinzhen@'     # 公钥
    # PUBLIC_KEY_PATH = 'xinzhengkeji@xzkj.com'  # 公钥
    PRIVATE_KEY = 'xinzhen@'    # 秘钥
    # PRIVATE_KEY_PATH = ''     # 秘钥

    # sha1 非对称加密
    def sha1(self,encode_str):
        self.sha1 = hashlib.sha1()
        self.sha1.update(encode_str.encode('utf-8'))
        return  self.sha1.hexdigest()


    """ DES 对称加密/解密 秘钥8位"""
    # pip install pyDes
    def des_encrypt(self,encode_str):
        iv = self.PRIVATE_KEY
        k = des(self.PRIVATE_KEY,CBC,iv,pad=None,padmode=PAD_PKCS5)
        encode = k.encrypt(encode_str,padmode=PAD_PKCS5)
        return binascii.b2a_hex(encode)

    def des_decrypt(self,encode_str):
        iv = self.PRIVATE_KEY
        k = des(self.PRIVATE_KEY, CBC, iv, pad=None, padmode=PAD_PKCS5)
        return  k.decrypt(binascii.a2b_hex(encode_str), padmode=PAD_PKCS5)


    """AES 对称加密/解密 建议秘钥16位 否则会补位"""
    # pip install pycrypto on lunix
    # pip install pycryptodome on windows
    def __add_to_16(self,key):
        while len(key) % 16 != 0 :
            key += '\0'
        return str.encode(key)

    def aes_encrypt(self,encode_str):
        aes = AES.new(self.__add_to_16(self.PRIVATE_KEY),AES.MODE_ECB)
        encrypt_aes = aes.encrypt(self.__add_to_16(encode_str))
        encrypt_text = str(base64.encodebytes(encrypt_aes),encoding='utf-8')
        return encrypt_text


    def aes_decrypt(self,encode_str):
        aes = AES.new(self.__add_to_16(self.PRIVATE_KEY), AES.MODE_ECB)
        base64_decrypted = base64.decodebytes(encode_str.encode(encoding='utf-8'))
        decrypted_text = str(aes.decrypt(base64_decrypted), encoding='utf-8').replace('\0','')
        return decrypted_text



