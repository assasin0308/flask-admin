# -*- coding:utf-8 -*-
# @Time: 2020/8/27 0027 9:48
# @Author: assasin
# @Email: assasin0308@sina.com
import time
import calendar
import redis
from utils.encrypt import EncryptStr


class TokenAuth(object):

    def __init__(self):
        self.__encrypt = EncryptStr()
        try:
            self.__redis = redis.StrictRedis(
                host='127.0.0.1',
                port=6379,
                db=0,
            )
        except Exception as connect_redis_err:
            print(connect_redis_err)


    def __make_token(self,token_str):
        return self.__encrypt.sha1(token_str + '_' + str(calendar.timegm(time.gmtime())) )

    def update_token(self,user_id,token_type='app'):
        token_string = self.__make_token(token_type + user_id )
        key = self.__redis.get(token_type + '_' + user_id)
        if key:
            self.__redis.delete(key)
        self.__redis.set(token_string,user_id)
        self.__redis.set(token_type + '_' + user_id,token_string)

        return token_string

    def check_token(self,token_str):
        token =  self.__redis.get(token_str)
        if token:
            return  token
        else:
            return False

     # 将验证码写入redis 200秒有效
    def write_roundstr(self,mobile,round_sms,expire_time):
        return self.__redis.setex(mobile,expire_time,round_sms.encode('utf-8').strip())


    # 校验 验证码是否有效
    def check_roundstr(self,mobile,round_sms):
        redis_sms = self.__redis.get(mobile)
        print(redis_sms)
        if not self.__redis.exists(mobile):
            return 1  # 验证码已过期
        if redis_sms != round_sms.encode('utf-8').strip():
            return 2 # 验证码错误
        return  3  # 验证OK

print(TokenAuth().check_token('113214244'))