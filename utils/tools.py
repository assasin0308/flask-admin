# -*- coding:utf-8 -*-
# @Time: 2020/8/27 0027 9:45
# @Author: assasin
# @Email: assasin0308@sina.com

import time,datetime
import calendar
import hashlib
from flask import jsonify,make_response



# return josn data
def render_result(result = None, errcode = 200, errmsg = 'success'):
    return_data = {
        'errcode': errcode,
        'errmsg': errmsg,
        'data': result
    }
    return jsonify(return_data)

# 将时间戳MD5加密
def random_str():
    m = hashlib.md5()
    time_str = str(calendar.timegm(time.gmtime())).encode('utf-8')
    m.update(time_str)
    return m.hexdigest()

# 时间戳转标准化时间
def time_format(time_stamp):
    # time_stamp = calendar.timegm(time.gmtime())
    timeArray = datetime.datetime.utcfromtimestamp(time_stamp)
    return timeArray.strftime('%Y-%m-%d %H:%M:%S')


def get_page_limit(page,per_page = 10):
    start = 0
    if  page:
        start = (page - 1) * per_page

    return " LIMIT  " + str(start) + "," + str(per_page)



# tree 结构
def xTree(datas,id = 'id',pid = 'parent_id',child_key = 'children'):
    lists = []
    tree = {}
    for item in datas:
        tree[item[id]] = item
    for i in datas:
        if not i[pid]:
            lists.append(tree[i[id]])
        else:
            parent_id = i[pid]
            if child_key not in tree[parent_id]:
                tree[parent_id][child_key] = []
            tree[parent_id][child_key].append(tree[i[id]])

    return lists