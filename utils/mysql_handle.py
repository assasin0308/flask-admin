# -*- coding:utf-8 -*-
# @Time: 2020/8/31 0031 10:58
# @Author: assasin
# @Email: assasin0308@sina.com


import pymysql
pymysql.install_as_MySQLdb()
from config.database import mysql_config


# 单例模式 MySQL数据库连接
class MysqlConn(object):
    __instance = None

    def __new__(cls, *args, **kwargs):
        if cls.__instance == None:
            cls.__instance = object.__new__(cls)
            return cls.__instance
        else:
            return cls.__instance

    def __init__(self,db_conn_params):
        self.db_host = db_conn_params.get('host')
        self.db_port = db_conn_params.get('port')
        self.db_user = db_conn_params.get('user')
        self.db_passwd = db_conn_params.get('password')
        self.db_name = db_conn_params.get('db_name')
        self.db_charset = db_conn_params.get('db_charset')

        try:
            self.connection = pymysql.connect(
                host=self.db_host,
                port=self.db_port,
                user=self.db_user,
                password=self.db_passwd,
                db=self.db_name,
                charset=self.db_charset,
                cursorclass=pymysql.cursors.SSDictCursor
            )
            if self.connection.open:
                self.cursor = self.connection.cursor()
        except Exception as conn_error:
            # print(conn_error)
            raise conn_error

     # fetchall
    def select(self,sql):
        try:
            self.cursor.execute(sql)
            rows = self.cursor.fetchall()
            # self.close()
            return rows
        except Exception as query_error:
            self.connection.rollback()
            self.close()
            print(query_error)

    # fetchone
    def row(self,sql):
        try:
            self.cursor.execute(sql)
            row = self.cursor.fetchone()
            # self.close()
            return row
        except Exception as query_error:
            self.connection.commit()
            self.close()
            print(query_error)

    # 获取条目数
    def num_rows(self,sql):
        try:
            self.cursor.execute(sql)
            row = self.cursor.fetchone()
            self.close()
            return row['count']
        except Exception as query_error:
            self.connection.commit()
            self.close()
            print(query_error)

    # update
    def update(self,sql):
        try:
            self.cursor.execute(sql)
            self.connection.commit()
            self.close()
            return self.cursor.lastrowid
        except :
            self.connection.rollback()
            self.close()

    # delete
    def delete(self,sql):
        try:
            affect_rows = self.cursor.execute(sql)
            self.connection.commit()
            self.close()
            return affect_rows
        except :
            self.connection.rollback()
            self.close()

     # truncate table
    def truncate(self,table_name):
        self.cursor.execute(" truncate table " + table_name )
        self.connection.commit()
        self.close()


    # close connection
    def close(self):
        self.cursor.close()
        self.connection.close()

    """......此处省略一万个方法"""



# conn = MysqlConn(MYSQL_DB_DEFAULT)
# lists_sql = "select SQL_CALC_FOUND_ROWS  a.id as user_id,a.username,a.email,a.role_id," \
#                     " r.rolename from administrator as a left join admin_role as r on a.role_id = r.id "
# db = MysqlConn(MYSQL_DB_DEFAULT)
# managers = db.select(lists_sql)
# count = db.row('select FOUND_ROWS() as count')
# print(managers,count)
