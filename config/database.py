# -*- coding:utf-8 -*-
# @Time: 2020/8/31 0031 11:00
# @Author: assasin
# @Email: assasin0308@sina.com

mysql_config = {
    'host' : '127.0.0.1',
    'port' : 3306,
    'user' : 'root',
    'password' : 'root',
    'db_name' : 'flask-admin',
    'db_charset' : 'utf8mb4'
}
