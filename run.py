# -*- coding:utf-8 -*-
# @Time: 2020/8/27 0027 9:25
# @Author: assasin
# @Email: assasin0308@sina.com

from flask import Flask,make_response,jsonify
from device import device_app

app = Flask(__name__)

# 设备模块
app.register_blueprint(device_app,url_prefix='/api')


@app.route('/',methods=['POST',"GET"])
def index():
    resp_data = {
        'error_code': 200,
        'error_msg': 'success,this is start page ',
        'data': ''
    }
    return make_response(jsonify(resp_data))


"""
error handler 400,404,405,500,502,504
"""


@app.errorhandler(400)
def bad_request(e):
    resp_data = {
        'error_code' : 400,
        'error_msg' : 'HTTP 400 - Bad Request',
        'data' : ''
    }
    return make_response(jsonify(resp_data)),400


@app.errorhandler(403)
def forbidden_request(e):
    resp_data = {
        'error_code' : 403,
        'error_msg' : '403 - Forbidden 禁止访问: 访问被拒绝',
        'data' : ''
    }
    return make_response(jsonify(resp_data)),403


@app.errorhandler(404)
def request_not_found(e):
    resp_data = {
        'error_code' : 404,
        'error_msg' : '404 - Page Not Found 未找到',
        'data' : ''
    }
    return make_response(jsonify(resp_data)),404

@app.errorhandler(405)
def request_method_forbidden(e):
    resp_data = {
        'error_code': 405,
        'error_msg': '405 - Method Not Allowed',
        'data': ''
    }
    return make_response(jsonify(resp_data)), 405



@app.errorhandler(500)
def request_internal_error(e):
    resp_data = {
        'error_code': 500,
        'error_msg': 'HTTP 500 - Internal Server Error ',
        'data': ''
    }
    return make_response(jsonify(resp_data)), 500


@app.errorhandler(502)
def request_badgateway_error(e):
    resp_data = {
        'error_code': 502,
        'error_msg': 'HTTP 502 - Bad Gateway 没有响应',
        'data': ''
    }
    return make_response(jsonify(resp_data)), 502


@app.errorhandler(504)
def request_gateway_timeout(e):
    resp_data = {
        'error_code': 504,
        'error_msg': 'HTTP 504 - Gateway Timeout 网关超时',
        'data': ''
    }
    return make_response(jsonify(resp_data)), 504


if __name__ == '__main__':
    print(app.url_map)
    app.run(host='0.0.0.0',port=5000,debug=True)



