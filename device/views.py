# -*- coding:utf-8 -*-
# @Time: 2020/8/27 0027 9:40
# @Author: assasin
# @Email: assasin0308@sina.com

from . import device_app
from utils.tools import render_result


@device_app.route('/login')
def login():
    resp_data = {
        'data': 'test data',
        'name': 'project test is OK '
    }
    return render_result(resp_data)